# (C) Copyright 2012
# Markus Hubig <mhubig@imko.de>
# IMKO GmbH <www.imko.de>
#
# This file is part of mata-imko a meta layer for the Yocto Project
# Embedded Linux build platform. 
# 
# meta-stamp9g20 is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
# 
# meta-stamp9g20 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with meta-stamp9g20. If not, see <http://www.gnu.org/licenses/>.

RRECOMMENDS_task-core-tools-profile_append_stamp9g20 = " systemtap"
RRECOMMENDS_task-core-tools-profile_append_portuxg20 = " systemtap"
