This README file contains information on building the meta-stamp9g20
BSP layer, and booting the images contained in the /binary directory.
Please see the corresponding sections below for details.


Dependencies
============

This layer depends on:

  URI: git://git.openembedded.org/bitbake
  branch: master

  URI: git://git.openembedded.org/openembedded-core
  layers: meta
  branch: master


Patches
=======

Please submit any patches against this BSP to the Yocto mailing list
(yocto@yoctoproject.org) and cc: the maintainer:

Maintainer: Markus Hubig <mhubig@imko.de>


Table of Contents
=================

I. Building the meta-stamp9g20 BSP layer
 II. Booting the images in /binary


I. Building the meta-stamp9g20 BSP layer
========================================

In order to build core-image-imko to clone the dependency layers first.

  $ git clone git://git.yoctoproject.org/poky
  $ cd poky
  $ git clone https://bitbucket.org/imko/meta-stamp9g20.git

Having done that, you can build a stamp9g20 image by adding the location
of the meta-stamp9g20 layer to bblayers.conf, along with any other layers
needed (to access common metadata shared between BSPs) e.g.:

BBLAYERS ?= " \
  /home/mhubig/Development/poky/meta \
  /home/mhubig/Development/poky/meta-yocto \
  /home/mhubig/Development/poky/meta-stamp9g20 \
  "

To enable the stamp9g20 layer, add the stamp9g20 MACHINE to local.conf:

MACHINE ?= "stamp9g20"

or

MACHINE ?= "portuxg20"

You should then be able to build a stamp9g20 image as such:

  $ source oe-init-build-env
  $ bitbake core-image-minimal

At the end of a successful build, you should have a live image that
you can boot from a USB flash drive (see instructions on how to do
that below, in the section 'Booting the images from /binary').


II. Booting the images in /binary
=================================

This BSP contains bootable live images, which can be used to directly
boot Yocto off of a USB flash drive.

Under Linux, insert a USB flash drive.  Assuming the USB flash drive
takes device /dev/sdf, use dd to copy the live image to it.  For
example:

# dd if=core-image-minimal-stamp9g20-20101207053738.hddimg of=/dev/sdf
# sync
# eject /dev/sdf

This should give you a bootable USB flash device.  Insert the device
into a bootable USB socket on the target, and power on. This should
result in a system booted to the linux terminal prompt.

If you want to ssh into the system, you can use the root terminal to
ifconfig the IP address and use that to ssh in. The root password is
empty, so to log in type 'root' for the user name and hit 'Enter' at
the Password prompt: and you should be in.

----

If you find you're getting corrupt images on the USB (it doesn't show
the syslinux boot: prompt, or the boot: prompt contains strange
characters), try doing this first:

# dd if=/dev/zero of=/dev/sdf bs=1M count=512
