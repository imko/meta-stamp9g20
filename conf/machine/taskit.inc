# (C) Copyright 2012
# Markus Hubig <mhubig@imko.de>
# IMKO GmbH <www.imko.de>
#
# This file is part of mata-imko a meta layer for the Yocto Project
# Embedded Linux build platform. 
# 
# meta-stamp9g20 is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
# 
# meta-stamp9g20 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with meta-stamp9g20. If not, see <http://www.gnu.org/licenses/>.

#@TYPE: Machine
#@NAME: stamp9g20 & portuxg20
#@DESCRIPTION: Machine configuration for the taskit stamp9g20 & portuxg20 systems

# Enable arm926ejs specific processor optimizations
include conf/machine/include/tune-arm926ejs.inc

# Ship all kernel modules by default
MACHINE_EXTRA_RRECOMMENDS = " kernel-modules"

# Allow for MMC booting
EXTRA_IMAGEDEPENDS += "u-boot at91bootstrap"

# Set the image types to create
IMAGE_FSTYPES += "tar.bz2 jffs2"
EXTRA_IMAGECMD_jffs2 = "-lnp "

# don't try to access tty1
USE_VT = "0"
SERIAL_CONSOLE = "115200 ttyS0"

# Kernel version to use
PREFERRED_PROVIDER_virtual/kernel ?= "linux-yocto"
PREFERRED_VERSION_linux-yocto ?= "3.2%"

# Kernel image format
KERNEL_IMAGETYPE = "uImage"

# u-boot build settings
UBOOT_ENTRYPOINT = "0x20008000"
UBOOT_LOADADDRESS = "0x20008000"

# some extra machine features
MACHINE_FEATURES = "usbgadget usbhost vfat"
