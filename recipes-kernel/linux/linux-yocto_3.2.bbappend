# (C) Copyright 2012
# Markus Hubig <mhubig@imko.de>
# IMKO GmbH <www.imko.de>
#
# This file is part of mata-imko a meta layer for the Yocto Project
# Embedded Linux build platform. 
# 
# meta-stamp9g20 is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
# 
# meta-stamp9g20 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with meta-stamp9g20. If not, see <http://www.gnu.org/licenses/>.

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

PR := "${PR}.1"

KEEPUIMAGE = "no"

COMPATIBLE_MACHINE_stamp9g20 = "stamp9g20"
KBRANCH_stamp9g20  = "standard/default/arm-versatile-926ejs"
KMACHINE_stamp9g20  = "stamp9g20"

COMPATIBLE_MACHINE_portuxg20 = "portuxg20"
KBRANCH_portuxg20  = "standard/default/arm-versatile-926ejs"
KMACHINE_portuxg20  = "portuxg20"

SRC_URI_append_portuxg20 = "\
        file://portuxg20-standard.scc   \
        file://portuxg20-preempt-rt.scc \
        file://portuxg20.scc            \
        "

SRC_URI_append_stamp9g20 = "\
        file://stamp9g20-standard.scc   \
        file://stamp9g20-preempt-rt.scc \
        file://stamp9g20.scc            \
        "
