# (C) Copyright 2012
# Markus Hubig <mhubig@imko.de>
# IMKO GmbH <www.imko.de>
#
# This file is part of mata-imko a meta layer for the Yocto Project
# Embedded Linux build platform. 
# 
# meta-stamp9g20 is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
# 
# meta-stamp9g20 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with meta-stamp9g20. If not, see <http://www.gnu.org/licenses/>.

require at91bootstrap_3.0.inc

COMPATIBLE_MACHINE_stamp9g20 = "stamp9g20"
COMPATIBLE_MACHINE_portuxg20 = "portuxg20"

AT91BOOTSTRAP_BOARD   ?= "${MACHINE}"
AT91BOOTSTRAP_VERSION ?= "3.0.1"

PR = "r3"

LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://COPYING;md5=715d652b71faa06628ddabd8270dbef0"

SRC_URI[md5sum] = "9426fc7962b68de70ec3941c3a6fd351"
SRC_URI[sha256sum] = "7449ea32ede2462fd81674770f1af19eddbf30f1d86bf0e2936ed59207c7db1d"

SRC_URI = "ftp://ftp.linux4sam.org/pub/Android4SAM/9m10g45/v1.1/patches/bootstrap30.tar.gz \
	file://0001-Update-.gitignore.patch \
	file://0002-Add-KConfig-support-for-booting-U-Boot.patch \
	file://0003-Generate-a-BOOT.BIN-file-instead-of-boot.bin.patch \
	file://0004-Add-support-for-dual-boot.patch \
	file://0005-Remove-old-afeb9260-files.patch \
	file://0006-Use-BOARD-instead-of-BOARDNAME-to-define-directory.patch \
	file://0007-Add-board-support-for-alternate-boot.patch \
	file://0008-Clean-up-printouts.patch \
	file://0009-Update-configs.patch \
	file://0010-Update-build-scripts.patch \
	file://0011-Fix-Cut-n-Paste-error.patch \
	file://0012-Fix-Cut-n-Paste-error-in-Makefile.patch \
	file://0013-Add-support-for-alternate-jump-address.patch \
	file://0014-Make-MAKENEW-useful.patch \
	file://0015-Update-configs.patch \
	file://0016-at91bootstrap-fix-build-error-in-openembedded-due-to.patch \
	file://0017-Change-switch-statements-to-if-statements-to-avoid-b.patch \
	file://0018-Added-support-for-Micron-MT29F1G08ABB-NAND-flash.patch \
	file://0019-Extracted-some-copyright-informations-from-main.c.patch \
	file://defconfig \
	"
