# (C) Copyright 2012
# Markus Hubig <mhubig@imko.de>
# IMKO GmbH <www.imko.de>
#
# This file is part of mata-imko a meta layer for the Yocto Project
# Embedded Linux build platform. 
# 
# meta-stamp9g20 is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
# 
# meta-stamp9g20 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with meta-stamp9g20. If not, see <http://www.gnu.org/licenses/>.

HOMEPAGE = "http://www.at91.com/linux4sam/bin/view/Linux4SAM/AT91Bootstrap"

SUMMARY = "Second programm loader (SPL) for ARM AT91 SoCs"
DESCRIPTION = "This SPL or bootloader get's loaded into internal SRAM \
by Atmels AT91 BootROM. It's usefull to start up the second bootloader \
mostly U-Boot."

SECTION = "bootloaders"
PROVIDES = "virtual/first-level-bootloader"

inherit deploy

COMPATIBLE_MACHINE = "(afeb9260|at91cap9adk|at91sam9g20ek|at91sam9rlek|\
                at91sam9263ek|at91sam9260ek|at91sam9xeek|at91sam9261ek)"

PARALLEL_MAKE = ""
PACKAGE_ARCH = "${MACHINE_ARCH}"
EXTRA_OEMAKE = "CROSS_COMPILE=${TARGET_PREFIX} DESTDIR=${DEPLOY_DIR_IMAGE} REVISION=${PR}"
FILESDIR = "${@os.path.dirname(d.getVar('FILE',1))}/at91bootstrap-3.0/${MACHINE}"

S = "${WORKDIR}/bootstrap30"

AT91BOOTSTRAP_SUFFIX  ?= "bin"
AT91BOOTSTRAP_FLASH   ?= "nandflash"
AT91BOOTSTRAP_VERSION ?= "3.0.1"
AT91BOOTSTRAP_BINARY  ?= "${MACHINE}-${AT91BOOTSTRAP_FLASH}boot-${AT91BOOTSTRAP_VERSION}-${PR}.${AT91BOOTSTRAP_SUFFIX}"
AT91BOOTSTRAP_IMAGE   ?= "at91bootstrap-${MACHINE}-${PV}-${PR}.${AT91BOOTSTRAP_SUFFIX}"
AT91BOOTSTRAP_SYMLINK ?= "at91bootstrap.${AT91BOOTSTRAP_SUFFIX}"

do_compile () {
	unset LDFLAGS
	unset CFLAGS
	unset CPPFLAGS

        oe_runmake mrproper
	rm -Rf ${S}/binaries
	cp ${S}/../defconfig ${S}/.config
        
        oe_runmake
}

do_install () {
        install -d ${D}/boot
        install ${S}/binaries/${AT91BOOTSTRAP_BINARY} ${D}/boot/${AT91BOOTSTRAP_IMAGE}
        ln -sf ${D}/boot/${AT91BOOTSTRAP_IMAGE} ${D}/boot/${AT91BOOTSTRAP_SYMLINK}
}

FILES_${PN} = "/boot"

do_deploy () {
        install -d ${DEPLOYDIR}
        install ${S}/binaries/${AT91BOOTSTRAP_BINARY} ${DEPLOYDIR}/${AT91BOOTSTRAP_IMAGE}

	cd ${DEPLOYDIR}
	rm -f ${AT91BOOTSTRAP_SYMLINK}
	ln -sf ${AT91BOOTSTRAP_IMAGE} ${AT91BOOTSTRAP_SYMLINK}
}

addtask deploy before do_build after do_compile
