# (C) Copyright 2012
# Markus Hubig <mhubig@imko.de>
# IMKO GmbH <www.imko.de>
#
# This file is part of mata-imko a meta layer for the Yocto Project
# Embedded Linux build platform. 
# 
# meta-stamp9g20 is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
# 
# meta-stamp9g20 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with meta-stamp9g20. If not, see <http://www.gnu.org/licenses/>.

# To build u-boot for your machine, provide the following lines in your machine
# config, replacing the assignments as appropriate for your machine.
# UBOOT_MACHINE = "portuxg20_config"
# UBOOT_ENTRYPOINT = "0x22000000"
# UBOOT_LOADADDRESS = "0x22000000"

LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://COPYING;md5=1707d6db1d42237583f50183a5651ecb \
                    file://README;beginline=1;endline=22;md5=78b195c11cb6ef63e6985140db7d7bab"

FILESDIR = "${@os.path.dirname(d.getVar('FILE',1))}/u-boot-git/${MACHINE}"

require u-boot.inc

# This revision corresponds to the tag "v2012.07". We use the revision in order
# to avoid having to fetch it from the repo during parse
SRCREV = "190649fb4309d1bc0fe7732fd0f951cb6440f935"

PV = "v2012.07+git${SRCPV}"
PR = "r1"

SRC_URI = "git://git.denx.de/u-boot.git;branch=master;protocol=git \
	file://0001-Enable-the-EMAC-clock-in-at91_macb_hw_init.patch \
	file://0002-at91-Add-support-for-taskit-AT91SAM9G20-boards.patch \
	file://0003-arm-Adds-board_postclk_init-to-the-init_sequence.patch \
	file://0004-Fixes-the-crippled-console-output-on-PortuxG20.patch \
	"

S = "${WORKDIR}/git"

PACKAGE_ARCH = "${MACHINE_ARCH}"
